package com.example.apiterremoto

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.apiterremoto.databinding.EqListItemBinding

private val TAG = EqAdapter::class.java.simpleName
class EqAdapter : ListAdapter<Eartquake, EqAdapter.EqViewHolder>(DiffCallback) {

    companion object DiffCallback : DiffUtil.ItemCallback<Eartquake>(){
        override fun areItemsTheSame(oldItem: Eartquake, newItem: Eartquake): Boolean {
            return  oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Eartquake, newItem: Eartquake): Boolean {
            return oldItem == newItem
        }

    }

    lateinit var  onItemClickListener: (Eartquake) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EqAdapter.EqViewHolder {
        val binding = EqListItemBinding.inflate(LayoutInflater.from(parent.context))
        return EqViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EqAdapter.EqViewHolder, position: Int) {
     val eartquake = getItem(position)
        holder.bind(eartquake)

    }

    inner class EqViewHolder(val binding: EqListItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(eartquake: Eartquake){
            binding.eqMagnitudText.text = eartquake.magnitude.toString()
            binding.eqPlaceText.text = eartquake.place
            binding.root.setOnClickListener {
                if(::onItemClickListener.isInitialized) {
                    onItemClickListener(eartquake)
                }else{
                    Log.e(TAG, "onItemClickListener not initialized")
                }

            }
        }
    }

}