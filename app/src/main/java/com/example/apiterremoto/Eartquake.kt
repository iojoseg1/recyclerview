package com.example.apiterremoto

data class Eartquake (val id:String, val place:String, val magnitude:Double, val time:Long,
                 val longitude: Double, val latitude: Double){
}