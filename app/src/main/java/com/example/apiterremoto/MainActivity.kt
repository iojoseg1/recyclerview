package com.example.apiterremoto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apiterremoto.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.eqRecycler.layoutManager = LinearLayoutManager(this)
        val eqList : MutableList<Eartquake> = mutableListOf<Eartquake>()
        eqList.add(Eartquake("1","Mexico",4.3,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("2","Guatemala",4.5,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("3","Honduras",3.3,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("4","EUA",4.3,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("5","Canada",2.3,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("6","Brasil",1.3,27092341,-102.4720,28.082912))
        eqList.add(Eartquake("7","Argentina",6.3,27092341,-102.4720,28.082912))

        val adapter = EqAdapter()
        binding.eqRecycler.adapter = adapter
        adapter.submitList(eqList)

        adapter.onItemClickListener = {
            Toast.makeText(this, it.place, Toast.LENGTH_LONG).show()
        }

        if(eqList.isEmpty()){
            binding.eqEmptyView.visibility = View.VISIBLE
        }else{
            binding.eqEmptyView.visibility = View.GONE
        }


    }
}